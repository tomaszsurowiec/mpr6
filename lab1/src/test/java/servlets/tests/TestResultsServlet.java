package servlets.tests;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import servlets.ResultsServlet;

public class TestResultsServlet extends Mockito{
	
	@Test
	public void servletShouldNotShowResultsThroughtGet() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		ResultsServlet servlet = new ResultsServlet();
		
        when(request.getParameter("capital")).thenReturn("");
        when(request.getParameter("credit")).thenReturn("diminishing");
        when(request.getParameter("numberOfInstalments")).thenReturn("");
        when(request.getParameter("percent")).thenReturn("");
        
        servlet.doGet(request, response);
        
        verify(response).sendRedirect("/");
	}

}
