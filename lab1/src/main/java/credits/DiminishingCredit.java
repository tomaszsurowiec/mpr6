package credits;

import static java.lang.Math.*;

public class DiminishingCredit {
		
		private double capitalInstalment;
		private double percentInstalment;
		private double sum;
		private double capital;
		private double numberOfInstalments;
		private double percent;
		
		public DiminishingCredit(double capital, double numberOfInstalments,
							  double percent)
		{
			this.capital=capital;
			this.numberOfInstalments=numberOfInstalments;
			this.percent=percent;
			this.capitalInstalment=this.capital/this.numberOfInstalments;
			this.percentInstalment=this.capital*this.percent/(12.0*100.0);
		}
		
		public double calculateInstalment(int m)
		{
			double instalment;
			instalment=((this.capital-((m-1)*this.capitalInstalment))*
					this.percent)/(12.0*100.0);
			return instalment;
		}
		
		private void calculateInstalment()
		{
			this.capitalInstalment=this.capital/this.numberOfInstalments;
			this.percentInstalment=this.capital*this.percent/(12.0*100.0);
		}
		
		private void CalculateSum()
		{
			double sum;
			sum=this.capital;
			int i=1;
			while (i<=this.numberOfInstalments)
			{
				sum=sum+this.calculateInstalment(i);
				i++;
			}
			this.sum=sum;
		}
		
		public double getCapitalInstalment() {
			this.calculateInstalment();
			return capitalInstalment;
		}
		
		public double getPercentInstalment() {
			this.calculateInstalment();
			return percentInstalment;
		}
		
		public double getSum() {
			this.CalculateSum();
			return sum;
		}
		
		public double getCapital() {
			return capital;
		}
		public void setCapital(double capital) {
			this.capital = capital;
		}
		public double getNumberOfInstalments() {
			return numberOfInstalments;
		}
		public void setNumberOfInstalments(double numberOfInstalments) {
			this.numberOfInstalments = numberOfInstalments;
		}
		public double getPercent() {
			return percent;
		}
		public void setPercent(double percent) {
			this.percent = percent;
		}
		
}
