package servlets;
import java.io.IOException;


import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import credits.ConstantCredit;
import credits.DiminishingCredit;


@WebServlet("/results")
public class ResultsServlet extends HttpServlet{
	
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
	 * 
	 */
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException
		{
			response.sendRedirect("/");
		}
		public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
		{
			String credit = request.getParameter("credit");
			String capital = request.getParameter("capital");
			String numberOfInstalments = request.getParameter("numberOfInstalments");
			String percent = request.getParameter("percent");
			
			double doubleCapital = Double.parseDouble(capital);
			double doubleNumberOfInstalments = Double.parseDouble(numberOfInstalments);
			double doublePercent = Double.parseDouble(percent);
			
			response.setContentType("text/html");
			String typeOfCredit;
			if (credit.equals("diminishing"))
				typeOfCredit="kredyt malej�cy";
			else
				typeOfCredit="kredyt sta�y";
			
			response.getWriter().println("<h1>Prezentujemy wybrany przez Ciebie  "
					+typeOfCredit
					+".</h1>");
			
			if (credit.equals("diminishing"))
			{
				DiminishingCredit credit2=new DiminishingCredit(doubleCapital,
						doubleNumberOfInstalments,doublePercent);
				double help,help2;
				int i=1;
				response.getWriter().println("<table><tr><td><b>Po�yczono(kapita�)"
						+ "</b></td><td>"
						+String.format( "%.2f", doubleCapital )
						+"</td></tr><tr><td><b>Procent"
						+ "</b></td><td>"
						+String.format( "%.2f", doublePercent )
						+"</td></tr><tr><td><b>Ilo�� rat"
						+ "</b></td><td>"
						+String.format( "%.2f", doubleNumberOfInstalments )
						+ "</td></tr></table><table>");
				response.getWriter().println("<tr><td><b>Nr raty</b></td>"
						+ "<td><b>Rata kapita�owa</b></td>"
						+ "<td><b>Rata odsetkowa</b></td>"
						+ "<td><b>Ca�kowita rata</b></td></tr>");
				while (i<=doubleNumberOfInstalments)
				{
					help=credit2.getCapitalInstalment();
					help2=credit2.calculateInstalment(i);
					response.getWriter().println("<tr><td>"
						+i
						+"</td><td>"
						+String.format( "%.2f", help )
						+" z�.</td><td>"
						+String.format( "%.2f", help2 )
						+" z�.</td><td>"
						+String.format( "%.2f", help+help2 )
						+" z�.</td><td></tr>");
					i++;
				}
				help=credit2.getSum();
				response.getWriter().println("</table>"
						+ "<table><tr><td> W sumie do sp�aty</td><td> "
						+String.format( "%.2f", help )
						+"</td></tr></table>");
			}
			else
			{
			
				ConstantCredit credit1=new ConstantCredit(doubleCapital,
									doubleNumberOfInstalments,doublePercent);
				double help;
				int i=1;
				response.getWriter().println("<table><tr><td><b>Po�yczono(kapita�)"
					+ "</b></td><td>"
					+String.format( "%.2f", doubleCapital )
					+"</td></tr><tr><td><b>Procent"
					+ "</b></td><td>"
					+String.format( "%.2f", doublePercent )
					+"</td></tr><tr><td><b>Ilo�� rat"
					+ "</b></td><td>"
					+String.format( "%.2f", doubleNumberOfInstalments )
					+ "</td></tr></table><table>");
				response.getWriter().println("<tr><td><b>Nr raty</b></td>"
					+ "<td><b>Rata</b></td></tr>");
				while (i<=doubleNumberOfInstalments)
				{
					help=credit1.getInstalment();
					response.getWriter().println("<tr><td>"
						+i
						+"</td><td>"
						+String.format( "%.2f", help )
						+" z�.</td></tr>");
					i++;
				}
				help=credit1.getSum();
				response.getWriter().println("</table>"
					+ "<table><tr><td> W sumie do sp�aty</td><td> "
					+String.format( "%.2f", help )
					+"</td></tr></table>");
			}
			
		}

}
