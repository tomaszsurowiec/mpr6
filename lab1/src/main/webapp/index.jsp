<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Kalkulator kredytowy</title>
	</head>
	<body>
		<h1>Wybierz swoj kredyt z naszym kalkulatorem bankowym</h1>
		<%-- Aby bylo metoda get: <form action="hello" method="get"> --%>
		<form method ="post" action="results">
			<div>
				<p>
					Wybierz jaki rodzaj kredytu chcesz wziac : &nbsp;&nbsp;&nbsp;
					<select name="credit" size=1>
						<option value="diminishing">
						Czy	Kredyt malejący/Rata malejąca?
						</option>
						<option value="constant">
							Czy Kredyt stały/Rata stała?
						</option>
					</select> 
				</p>
				<p>
					Wpisz ile ma wynosić mamy porzyczyc ci pieniedzy :):&nbsp;&nbsp;&nbsp;
					<input type="number" name="capital" min="100" required />
				</p>
				<p>
					W ile miesiecy chcesz splacic kredyt:&nbsp;&nbsp;&nbsp;
					<input type="number" name="numberOfInstalments" min="6" required />
				</p>
				<p>	
					Jakie oprocentowanie?:&nbsp;&nbsp;&nbsp;
					<input type="number" name="percent" step="any" min="0.1" required />
				</p>
				<br/>
				<input type="submit" value="Oblicz"/>
 
			</div>

		</form>

	</body>
</html>