
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import domain.Address;
import domain.Permission;
import domain.Person;
import domain.Role;
import domain.User;
import service.UserService;


public class Main {
	private static List<User> users;

	private static void init() {
		
		Person Tomasz = new Person();
		
		Person Maria = new Person();
		
		Person Anrzej = new Person();
		
		
		Tomasz.setAge(19);
		Tomasz.setName("Tomasz");
		Tomasz.setSurname("Suro");
		
		Anrzej.setAge(26);
		Anrzej.setName("Andrzej");
		Anrzej.setSurname("Stefaniuk");
		
		
		Maria.setAge(67);
		Maria.setName("Maria");
		Maria.setSurname("Sawicka");
		
		User user1 = new User("Tsuro", "zabawawjave", Tomasz);
		User user2 = new User("Andrz", "pasikonik", Anrzej);
		User user3 = new User("MarS", "pass123", Maria);
		
		
		users = Arrays.asList(user1, user2, user3);
		
		Role roleUser = new Role();
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		Tomasz.setRole(roleAdmin);
		Maria.setRole(roleGuest);
		Anrzej.setRole(roleGuest);
	
		
		Address a1 = new Address("Polska", "Gdansk", "Garncarska", 333);
		Address a2 = new Address("Poland", "Sopot", "Kawalerow",666);
		Address a3 = new Address("Poland", "Gdynia", "Buraczna", 567);
		
		Tomasz.setAddresses(Arrays.asList(a1,a2,a3));
		Maria.setAddresses(Arrays.asList(a1,a2));
		Anrzej.setAddresses(Arrays.asList(a1));
		
		Permission permissionWrite = new Permission();
		Permission permissionRead = new Permission();
		Permission permissionExecute = new Permission();
		
		permissionWrite.setName("write");
		permissionRead.setName("read");
		permissionExecute.setName("execute");
		
		List<Permission> permmissionUser = Arrays.asList(permissionWrite,permissionRead);
		List<Permission> permmissionGuest = Arrays.asList(permissionRead);
		List<Permission> permmissionAdmin = Arrays.asList(permissionWrite,permissionRead,permissionExecute);
		
		roleUser.setPermissions(permmissionUser);
		roleGuest.setPermissions(permmissionGuest);
		roleAdmin.setPermissions(permmissionAdmin);
		List<User> moreThan2Addresses = UserService.findUsersWhoHaveMoreThanOneAddress(users);
		System.out.println(moreThan2Addresses);
		
		Person oldest = UserService.findOldestPerson(users);
		System.out.println("Osoba Najstarsza to: " + oldest.getName() + " " 
							+ oldest.getSurname() + " lat " + oldest.getAge());
		User longest = UserService.findUserWithLongestUsername(users);
		System.out.println("Najdluzszy login ma: " + longest.getName());
		
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		System.out.println("Kto moze pic piwo poniewaz ma lat 18:" + result);
		
		List<String> nameStartingWithA = UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
		System.out.println("uprawnienia osob z imieniem na A: ");
		System.out.println(nameStartingWithA);

		System.out.println("Lista uprawnien na S: ");
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
		
		Map<Role, List<User>> groupUsersByRole = UserService.groupUsersByRole(users);
		System.out.println(groupUsersByRole);
		
		Map<Boolean, List<User>> groupUsersBy18 = UserService.partitionUserByUnderAndOver18(users);
		System.out.println(groupUsersBy18);
	
	}
	


	public static void main(String[] args) {
		init();
	
	}
}
