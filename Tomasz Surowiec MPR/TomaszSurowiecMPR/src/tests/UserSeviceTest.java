package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;



import domain.Address;
import domain.Permission;
import domain.Person;
import domain.Role;
import domain.User;
import service.UserService;

public class UserSeviceTest {

	@Test
	public void testFindUsersWhoHaveMoreThanOneAddress() {
		
		Person a = new Person();
		Person b = new Person();
		
		User u = new User("name", "Password", a);
		User UserWith2Address = new User("name2", "haslo", b);
		
		Address a1 = new Address("Poland", "Koszalin", "Gierczak", 127);
		Address a2 = new Address("Poland", "Cewlino", "Cewlino", 15);
		
		a.setAddresses(Arrays.asList(a1));
		b.setAddresses(Arrays.asList(a1,a2));
		
		u.setPersonDetails(a);
		UserWith2Address.setPersonDetails(b);
		
		List<User> users = new ArrayList<User>();
		users.add(u);
		users.add(UserWith2Address);

		List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);

		assertSame(result.get(0), UserWith2Address); 
		
	}

	@Test
	public void testFindOldestPerson() {
		List<User> users;

		Person Tomasz = new Person();
		Person Anrzej = new Person();
		Person Maria = new Person();
		Tomasz.setAge(23);
		Anrzej.setAge(22);
		Maria.setAge(52);
		Maria.setName("Maria");
		Maria.setSurname("Sawicka");
		
		User user1 = new User("Tsuro", "zabawawjave", Tomasz);
		User user2 = new User("Andrz", "pasikonik", Anrzej);
		User user3 = new User("MarS", "pass123", Maria);

		users = Arrays.asList(user1, user2, user3);
		
		Person result = UserService.findOldestPerson(users);
		assertSame(82, result.getAge());
		
	}

	@Test
	public void testFindUserWithLongestUsername() {
		List<User> users;

		Person Tomasz = new Person();
		Person Anrzej = new Person();
		Person Maria = new Person();
		Tomasz.setAge(19);
		Anrzej.setAge(26);
		Maria.setAge(67);
		Maria.setName("Maria");
		Maria.setSurname("Sawicka");
		
		User user1 = new User("Tsuro", "zabawawjave", Tomasz);
		User user2 = new User("Andrz", "pasikonik", Anrzej);
		User user3 = new User("MarS", "pass123", Maria);

		users = Arrays.asList(user1, user2, user3);
		User result = UserService.findUserWithLongestUsername(users);
		assertSame("saviola", result.getName());
	}

	@Test
	public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() {
		
		List<User> users;
		

		Person Tomasz = new Person();
		Person Anrzej = new Person();
		Person Maria = new Person();
		
		Tomasz.setAge(19);
		Tomasz.setName("Tomasz");
		Tomasz.setSurname("Suro");
		
		Anrzej.setAge(26);
		Anrzej.setName("Andrzej");
		Anrzej.setSurname("Stefaniuk");
		
		Maria.setAge(67);
		Maria.setName("Maria");
		Maria.setSurname("Sawicka");
		
		User user1 = new User("Tsuro", "zabawawjave", Tomasz);
		User user2 = new User("Andrz", "pasikonik", Anrzej);
		User user3 = new User("MarS", "pass123", Maria);

		users = Arrays.asList(user1, user2, user3);
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		
		assertEquals("Kamil Kubicki,Artur Wozniak,Janusz Sawada", result);
	}
	

	@Test
	public void testGetSortedPermissionsOfUsersWithNameStartingWithA() {
		
		Person Tomasz = new Person();
		Person Anrzej = new Person();
		Person Maria = new Person();
		
		Tomasz.setAge(19);
		Tomasz.setName("Tomasz");
		Tomasz.setSurname("Suro");
		
		Anrzej.setAge(26);
		Anrzej.setName("Andrzej");
		Anrzej.setSurname("Stefaniuk");
		
		
		Maria.setAge(67);
		Maria.setName("Maria");
		Maria.setSurname("Sawicka");
		
		User user1 = new User("Tsuro", "zabawawjave", Tomasz);
		User user2 = new User("Andrz", "pasikonik", Anrzej);
		User user3 = new User("MarS", "pass123", Maria);
		List<User> users = Arrays.asList(user1, user2,user3);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		Maria.setRole(roleGuest);
		Anrzej.setRole(roleGuest);
		Tomasz.setRole(roleAdmin);
		
		Permission permissionWrite = new Permission();
		Permission permissionRead = new Permission();
		Permission permissionExecute = new Permission();
		
		permissionRead.setName("read");
		
		permissionExecute.setName("execute");
		
		permissionWrite.setName("write");
		
		List<Permission> permGuest = Arrays.asList(permissionRead);
		List<Permission> permAdmin = Arrays.asList(permissionExecute,permissionRead,permissionWrite);
		
		roleGuest.setPermissions(permGuest);
		roleAdmin.setPermissions(permAdmin);
		
		assertEquals(Arrays.asList("read"),UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users));
		
	}

	@Test
	public void testGroupUsersByRole() {
		List<User> users;
		
		Person Tomasz = new Person();
		Person Anrzej = new Person();
		Person Maria = new Person();
		
		Tomasz.setAge(19);
		Tomasz.setName("Tomasz");
		Tomasz.setSurname("Suro");
		
		Anrzej.setAge(26);
		Anrzej.setName("Andrzej");
		Anrzej.setSurname("Stefaniuk");
		
		
		Maria.setAge(67);
		Maria.setName("Maria");
		Maria.setSurname("Sawicka");
		
		User user1 = new User("Tsuro", "zabawawjave", Tomasz);
		User user2 = new User("Andrz", "pasikonik", Anrzej);
		User user3 = new User("MarS", "pass123", Maria);
		
		List<User> guestList = Arrays.asList(user3);
		List<User> adminList = Arrays.asList(user1);
		users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		Maria.setRole(roleGuest);
		Anrzej.setRole(roleGuest);
		Tomasz.setRole(roleAdmin);
		
		Map<Role, List<User>> result = UserService.groupUsersByRole(users);
		Map<Role, List<User>> test = new HashMap<Role, List<User>>();
		test.put(roleGuest, guestList );
		test.put(roleAdmin, adminList);
		assertEquals(test,result);
	}
	

	@Test
	public void testPartitionUserByUnderAndOver18() {
List<User> users;
		
	Person Tomasz = new Person();
	Person Anrzej = new Person();
	Person Maria = new Person();

	Tomasz.setAge(19);
	Tomasz.setName("Tomasz");
	Tomasz.setSurname("Suro");

	Anrzej.setAge(26);
	Anrzej.setName("Andrzej");
	Anrzej.setSurname("Stefaniuk");


	Maria.setAge(67);
	Maria.setName("Maria");
	Maria.setSurname("Sawicka");

		User user1 = new User("Tsuro", "zabawawjave", Tomasz);
		User user2 = new User("Andrz", "pasikonik", Anrzej);
		List<User> under18List = Arrays.asList(user1);
		List<User> over18List = Arrays.asList(user2);
		users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		Maria.setRole(roleGuest);
		Anrzej.setRole(roleGuest);
		Tomasz.setRole(roleAdmin);
		
		Map<Boolean, List<User>> result = UserService.partitionUserByUnderAndOver18(users);
		Map<Boolean, List<User>> test = new HashMap<Boolean, List<User>>();
		test.put(false, under18List );
		test.put(true, over18List);
		assertEquals(test,result);
	}

}
